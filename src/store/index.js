import {createStore} from 'redux';

/*
  testPushStore(event){
    store.dispatch({type: 'coupeNummer', payload: "holyshit nigga"})
    event.preventDefault();           ==alleen wanneer button
  }

*/

const initialState={
    vertrekStation:"",
    aankomstStation:"",
    vertrekTijd:"",
    aankomstTijd:"",
    coupeNummer: "",
    klachtKeuze:"",
    klachtBeschrijving:"",
    verdieping:"",

};

const reducer = (state = initialState, action)=>{
    console.log(action)
    switch(action.type){
        case "vertrekStation":
            state.vertrekStation = action.payload;
        break;
        case "aankomstStation":
            state.aankomstStation = action.payload;
        break;
        case "vertrekTijd":
            state.vertrekTijd = action.payload;
        break;
        case "aankomstTijd":
            state.aankomstTijd = action.payload;
        break;
        case "coupeNummer":
            state.coupeNummer = action.payload;
        break;
        case "klachtKeuze":
            state.klachtKeuze = action.payload;
        break;
        case "klachtBeschrijving":
            state.klachtBeschrijving = action.payload;
        break;
        case "verdieping":
            state.verdieping = action.payload;
        break;
        default:
            console.log("No values changed because no type selected")
    }
    return state;
}

const store = createStore(reducer);
export default store;
