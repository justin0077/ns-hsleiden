import React from 'react'

import './Layout.css'

const layout = (props) => ( 
    <>
        {
            props.background ? 
            <div className="frontend-body">
                <div className="container">
                    <div className="frontend-layout">
                        {props.children}
                    </div>
                </div>
            </div> 
            : <div className="container">
                <div className="frontend-layout">
                    {props.children}
                </div>
            </div>
        } 
    </>
)

export default layout