class Auth {
    constructor() {
        this.authenticated = false
        this.token = ""
    }

    login(t, cb) {
        this.authenticated = true
        this.token = t
        cb();
    }

    logout(cb) {
        this.authenticated = false
        localStorage.clear()
        cb();
    }

    isAuthenticated() {
        return this.authenticated
    }

    getToken() {
        return this.token;
    }

}

export default new Auth();