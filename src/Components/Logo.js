import React from 'react'
import classes from './logo.module.css'

const logo = (props) => (
    <img className={classes.logo} src="/images/logo.png" alt="logo" />
)

export default logo;