import React from 'react'
import classes from './Input.module.css'

const input = (props) => (    
    <div className={`mb-3 input-group ${classes.group}`}>
        
        {
            props.logo ?  <div className="input-group-prepend"><span className={`input-group-text ${classes.text}`} id="basic-addon1">
            <i className="material-icons login">
                {props.logo}
            </i>
            </span> 
        </div> :
            null
        }
        
        <input name={props.name}
            className="form-control"
            type={props.type}
            placeholder={props.placeholder}
            autoComplete={props.autocomplete}
            onChange={props.change} 
            required={props.required}
            maxLength={props.max}/>
    </div>
)

export default input