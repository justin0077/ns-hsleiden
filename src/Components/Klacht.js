import React, { Component } from 'react';
import Coupe from './Coupé';
import Route from './Route.js';
import '../Pages/Home/Home.css';
import Layout from '../hoc/Layout';
import Logo from './Logo'
import Button from './UI/Button/Button';
import Dropdown from './Dropdown';
import Summary from "../Pages/Summary.js";
import store from '../store';


class Klacht extends Component {

  constructor(props) {
    super(props)
    this.state = {
      currentStep: 1,
      email: '',

      coupeNummer: '',
    }
  }


  handleChange = event => {
    const { name, value } = event.target
    this.setState({
      [name]: value 
    })
  }

  complaint_next = (event) => {
    event.preventDefault();
    let currentStep = this.state.currentStep
    currentStep = currentStep >= 3 ? 4 : currentStep + 1
    this.setState({
      currentStep: currentStep
    })
  }

  complaint_prev = (event) => {
    event.preventDefault();
    let currentStep = this.state.currentStep
    currentStep = currentStep <= 1 ? 1 : currentStep - 1
    this.setState({
      currentStep: currentStep
    })
  }

     anchorTag1() {
          let currentStep = this.state.currentStep;
          if(currentStep === 1) {
            return(
              <a className="nav-link active" href="#Rit">Rit</a>
            )
          }
          return(
            <a className="nav-link" href="#Rit">Rit</a>
          )
        }

        anchorTag2() {
          let currentStep = this.state.currentStep;
          if(currentStep === 2) {
            return(
              <a className="nav-link active" href="#Coupé">Coupé</a>
            )
          }
          return(
            <a className="nav-link" href="#Coupé">Coupé</a>
          )
        }

        anchorTag3() {
          let currentStep = this.state.currentStep;
          if(currentStep === 3) {
            return(
              <a className="nav-link active" href="#Klacht">Klacht</a>
            )
          }
          return(
            <a className="nav-link" href="#Klacht">Klacht</a>
          )
        }

        anchorTag4() {
          let currentStep = this.state.currentStep;
          if(currentStep === 4) {
            return(
              <a className="nav-link active" href="#Verstuur">Verstuur</a>
            )
          }
          return(
            <a className="nav-link" href="#Verstuur">Verstuur</a>
          )
        }



  /*
  * the functions for our button
  */
  previousButton() {
    let currentStep = this.state.currentStep;
    if (currentStep !== 1) {
      return (
        <Button variant="back" clicked={this.complaint_prev} type="button">
          <i className="material-icons back">
            arrow_back
          </i>
        </Button>
      )
    }
    return null;
  }

  nextButton() {
    let currentStep = this.state.currentStep;
    if (currentStep < 4) {
      return (
        <Button variant="next" clicked={this.complaint_next} type="button">Volgende</Button>
      )
    }
    return null;
  }

  render() {
    return (
      <Layout background>
            <Logo />
            <h1>Klachten formulier</h1>

            <ul className="nav nav-tabs klachten">
              <li className="nav-item">
              {this.anchorTag1()}
              </li>
              <li className="nav-item">
              {this.anchorTag2()}
              </li>
              <li className="nav-item">
              {this.anchorTag3()}
              </li>
              <li className="nav-item">
              {this.anchorTag4()}
              </li>
            </ul>
            <form className="complaint-form" onSubmit={this.handleSubmit}>
              {/*
                    render the form steps and pass required props in
                    */}
              <Step1
                currentStep={this.state.currentStep}
                handleChange={this.handleChange}
                //email={this.state.email}
              />
              <Step2
                currentStep={this.state.currentStep}
                handleChange={this.handleChange}
                //username={this.state.username}
              />
              <Step3
                currentStep={this.state.currentStep}
                handleChange={this.handleChange}
                //password={this.state.password}
              />
              <Step4
                currentStep={this.state.currentStep}
                handleChange={this.handleChange}
                //password={this.state.password}
              />
              <div className="button-container">
                {this.previousButton()}
                {this.nextButton()}
              </div>

            </form>
      </Layout>
    );
  }
}

function Step1(props) {
  if (props.currentStep !== 1) {
    return null
  }
  return (
    <div className="Route">
      <Route />
    </div>
  );
}

function Step2(props) {
  if (props.currentStep !== 2) {
    return null
  }
  return (
    <Coupe />
  );
}

function Step3(props) {

  if (props.currentStep !== 3) {
    return null
  }
    return (
      <Dropdown />
  );
}

function Step4(props) {
  if (props.currentStep !== 4) {
    return null
  }
  return (
    <Summary store={store} />
  );
}

export default Klacht
