import React, { Component } from 'react'
import '../Pages/Home/Home';
import store from '../store';
import Input from '../Components/UI/Input/Input';
import Button from './UI/Button/Button';

class Coupé extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coupeNummer: '0000'
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({coupeNummer: event.target.value});
    store.dispatch({type: 'coupeNummer', payload: event.target.value})
  }

  setPosition(event) {
    store.dispatch({type: 'verdieping', payload: event.target.value})
    console.log(event.target.value);
  }

  render() {
    return (
        <div>
        <label>
          <h2>Treinstel nummer:</h2>
          <Input
            type="text"
            placeholder="0000"
            change={this.handleChange}
            max="4"
          />
          <hr></hr>
          <h2>Selecteer uw positie in de Coupé</h2>
          <div className="btn-group btn-group-toggle mx-auto" onClick={this.setPosition.bind(this)}>
            <label>
              <input className="btn btn-primary active-white mr-4" id="optie1" type="button" value="Boven" name="positie"/>
            </label>
            <label>
              <input className="btn btn-primary active-white" id="optie2" type="button" value="Beneden" name="positie"/>
            </label>
            </div>
        </label>
        </div>
    );
  }
}

export default Coupé;
