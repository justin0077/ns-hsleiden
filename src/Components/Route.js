import React from 'react';
import ReactDOM from 'react-dom'
import axios from 'axios';
import '../Pages/Home/Home.css';
import store from '../store';
import Suggestions from './Suggestions'
import Trains from './Trains'

const BASE_URL = 'https://api.nsklacht.nl/api/station'

class Route extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
        vanStation:"",
        naarStation:"",
        vanCode: "",
        naarCode: "",
        term: '',
        results: [],
        trains: [],
    };

    this.naarStationChange = this.naarStationChange.bind(this);
    this.vanStationChange = this.vanStationChange.bind(this);
  }

  getInfo = () => {
    axios.get(`${BASE_URL}?term=${this.state.term}`)
      .then((res) => {
        this.setState({ results: res.data })
      })
  }

  getTrips = (from, to) =>{
    var DateTim = new Date();

    var startDateTimeInMilliseconds = DateTim.getTime();
    var minutesInMilliseconds = 30*60*1000;   // milliseconds

    var newDateTime = new Date(startDateTimeInMilliseconds + minutesInMilliseconds);

    var str = newDateTime.getFullYear() + "-" + ('0' + (newDateTime.getMonth() + 1)).slice(-2) + "-" + ('0' + newDateTime.getDate()).slice(-2) + "T" + ('0' + newDateTime.getHours()).slice(-2) + ":" + ('0' + newDateTime.getMinutes()).slice(-2) + ":" + ('0' + newDateTime.getSeconds()).slice(-2);

    if((!(from === "" || to === ""))){
      axios.get('https://gateway.apiportal.ns.nl/public-reisinformatie/api/v3/trips?fromStation='+ from + '&toStation=' + to + '&dateTime=' + str, {
        headers: {'Ocp-Apim-Subscription-Key': 'be86842b967a407f91c2a698dc05e77b'}})
        .then(res => {
          console.log(res);
          this.setState({trains: res.data.trips})
        }).catch(e => {
          console.log(e.res);
        })
    }
  }

  vanStationChange(event) {
    this.setState({vanStation: event.target.value});
    this.setState({
      term: event.target.value
    }, () => {
      if (this.state.term && this.state.term.length > 1) {
        if (this.state.term.length % 2 === 0) {
          this.getInfo()
        }
      } else if (!this.state.term) {
      }
    })
  }
  naarStationChange(event) {
    this.setState({naarStation: event.target.value});
    this.setState({
      term: event.target.value
    }, () => {
      if (this.state.term && this.state.term.length > 1) {
        if (this.state.term.length % 2 === 0) {
          this.getInfo()
        }
      } else if (!this.state.term) {
      }
    })
  }

  vanStationClicked = (station, code) => {
    this.setState({vanStation: station, vanCode: code});
    store.dispatch({type: 'vertrekStation', payload: station});
    this.getTrips(code, this.state.naarCode);
  }

  naarStationClicked = (station, code) => {
    this.setState({naarStation: station, naarCode: code});
    store.dispatch({type: 'aankomstStation', payload: station});
    this.getTrips(this.state.vanCode, code);
  }

  wissel(){
       store.dispatch({type: 'vertrekStation', payload: this.state.naarStation})
       store.dispatch({type: 'aankomstStation', payload: this.state.vanStation})

        var wisselVar = this.state.vanStation;
        this.setState({
          vanStation:this.state.naarStation,
          naarStation: wisselVar,
        })
        this.getTrips(this.state.naarStation, this.state.vanStation)
    }

    stationClicked = (originTime, destinationTime) => {
      store.dispatch({type: "vertrekTijd", payload: originTime});
      store.dispatch({type: "aankomstTijd", payload: destinationTime});
    }

  onSubmit = (event) => {
    event.preventDefault();
}

  render(){

    const suggestionFrom = document.activeElement === ReactDOM.findDOMNode(this.refs.stationFrom) ? <Suggestions clicked={this.vanStationClicked} results={this.state.results} /> : null
    const suggestionTo = document.activeElement === ReactDOM.findDOMNode(this.refs.stationTo) ? <Suggestions clicked={this.naarStationClicked} results={this.state.results} /> : null
    return(
      <div className='Route' onSubmit = {this.onSubmit}>
      <h2>Selecteer uw rit</h2>
        <div className="ride">
            <div className="input-group adres-input mb-3">
                <div className="input-group-prepend from-to">
                    <span className="input-group-text from-to" id="basic-addon1">
                        van
                    </span>
                </div>
                <input
                    name="from"
                    ref="stationFrom"
                    className="form-control from-input"
                    type="text"
                    placeholder="station"
                    required
                    value={this.state.vanStation}
                    onChange={this.vanStationChange}
                    autoComplete="off"
                    />
                <span className="input-group-text change" id="basic-addon1"  onClick={() => this.wissel()}>
                    <i className="material-icons change" >
                        swap_vert
                    </i>
                </span>
            </div>
            {suggestionFrom}
            <div className="input-group adres-input mb-3">
              <div className="input-group-prepend from-to">
                <span className="input-group-text from-to" id="basic-addon1">
                    naar
                </span>
              </div>
            <input
            name="to"
            ref="stationTo"
            className="form-control from-input to-input"
            type="text"
            placeholder="station"
            required
            value={this.state.naarStation}
            onChange={this.naarStationChange}
            autoComplete="off"/>
            <span className="input-group-text change" id="basic-addon1"></span>
          </div>
        {suggestionTo}
        </div>
        <ul className="choose-ride">
          <Trains results={this.state.trains} clicked={this.stationClicked}/>
        </ul>
      </div>
    )
  }
};

export default Route;
