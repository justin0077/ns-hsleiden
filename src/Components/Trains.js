import React from 'react';
import classes from './Trains.module.css'

const Train = (props) => {
    const option = props.results.map(r => {
        
        var originDate = new Date(r.legs[0].origin.plannedDateTime);
        var originTime = ("0" + originDate.getHours()).slice(-2) + ":" + ("0" + originDate.getMinutes()).slice(-2);
        
        var destinationDate = new Date(r.legs[0].destination.plannedDateTime);
        var destinationTime = ("0" + destinationDate.getHours()).slice(-2) + ":" + ("0" + destinationDate.getMinutes()).slice(-2);
        


        return (
            <li
                key={r.checksum}
                onClick={() => props.clicked(originTime, destinationTime)}>
                <p>{originTime} </p>
                <i className="material-icons from_to">
                    keyboard_arrow_right
                </i>
                <p>{destinationTime}</p>
            </li>
        )
    })

    return <ul className={classes.Trains}>{option}</ul>
  }
  
  export default Train