import React, { Component } from 'react'
import axios from 'axios'
import Suggestions from './Suggestions'

const BASE_URL = 'https://api.nsklacht.nl/api/station'

class Searchbar extends Component {
  state = {
    term: '',
    results: []
  }

  getInfo = () => {
    axios.get(`${BASE_URL}?term=${this.state.term}`)
      .then((res) => {
        this.setState({ results: res.data })
      })
  }

  handleInputChange = () => {
    this.setState({
      term: this.search.value
    }, () => {
      if (this.state.term && this.state.term.length > 1) {
        if (this.state.term.length % 2 === 0) {
          this.getInfo()
        }
      } else if (!this.state.term) {
      }
    })
  }

  suggestionClicked = (code) => {
    console.log(code);
  }

  render() {
    return (
      <form>
        <input
          placeholder="Search for..."
          ref={input => this.search = input}
          onChange={this.handleInputChange}
        />
        <Suggestions clicked={this.suggestionClicked} results={this.state.results} />
      </form>
    )
  }
}

export default Searchbar;
