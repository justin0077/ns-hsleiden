import React from 'react';
import Select from 'react-select';
import store from "../store"

const options = [
  { value: 'Klacht 1', label: 'Geluidsoverlast' },
  { value: 'Klacht 2', label: 'Aanranding' },
  { value: 'Klacht 3', label: 'Overtollig afval' },
  { value: 'Klacht 4', label: 'Trein staat stil' },
  { value: 'Klacht 5', label: 'Overig' },
];

class Dropdown extends React.Component {
  state = {
    selectedOption: null,
  };

  handleKeuzeChange = selectedOption => {
    this.setState({ selectedOption });
    console.log(selectedOption.label);
    store.dispatch({type: 'klachtKeuze', payload: selectedOption.label})
  };
  
  handleBeschrijvingChange(event){
    console.log(event.target.value)
    store.dispatch({type: 'klachtBeschrijving', payload: event.target.value})
  }

  render() {
    const { selectedOption } = this.state;
    return (
      <div>
        <h2>Geef uw klacht op:</h2>
        <Select
          value={selectedOption}
          onChange={this.handleKeuzeChange}
          options={options}
          styles={{container: (provided) => ({
            ...provided,
            width: '85%',
            margin: '0 auto',
            marginTop: 0
          })}}
        />

      <div>
        <div className="form-group ">
          <textarea onChange={this.handleBeschrijvingChange} className="form-control complaint-text" id="exampleFormControlTextarea1" placeholder="Omschrijf de klacht"></textarea>
        </div>
      </div>
    </div>
    );
  }
}

export default Dropdown
