import React, { useState } from 'react'
import {Link} from 'react-router-dom'

const AdminNav = (props) => {
    const [trein, setTrein] = useState(0);

    const onSubmit = (e) => {
        e.preventDefault();
        console.log(trein);
        props.toTrain(trein)
    }

    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container">
                    <Link to="/admin" className="navbar-brand">NS klachten</Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
            
                    <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li className="nav-item active">
                            <Link className="nav-link" to="/admin">Home</Link>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#/" onClick={props.logout}>Uitloggen</a>
                        </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    )
}


export default AdminNav