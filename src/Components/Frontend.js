import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import './frontend-module.css';
import Layout from '../hoc/Layout'
import Button from '../UI/Button/Button'
import Input from '../UI/Input/Input'

class Frontend extends Component {


    render() {
        return (
            <Layout background>
                <img className="frontend-logo" src="/images/logo.png" alt="logo" />
                <h1 className="title">Welkom bij de NS Overlast Melder</h1>
                <h2>Vul hier uw e-mail in en meldt de overlast</h2>
                <form className="frontend-form" onSubmit={this.onSubmit}>
                    <Input
                        name="e-mail"
                        logo="email"
                        type="text"
                        placeholder="E-mail"
                        required
                    />

                    <div className="form-group" style={{ marginTop: '150px' }}>
                        <Link to="/klacht"><Button variant="blue" clicked={this.onSubmit} type="submit">Volgende</Button></Link>
                    </div>

                </form>
            </Layout>
        );
    }
}

export default Frontend
