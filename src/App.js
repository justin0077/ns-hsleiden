import React from 'react';
import { Route, Switch, Link } from 'react-router-dom'
import { ProtectedRoute } from './hoc/ProtectedRoute'
import Login from './Pages/Login/Login'
import Register from './Pages/Register/Register'
import Frontend from './Pages/Home/Home';
import Klacht from './Components/Klacht';
import Admin from './Pages/Admin/Admin'
import ResetPassword from './Pages/Reset/ResetPassword';
import Forgotten from './Pages/Forgotten/Forgotten';
import thanks from './Pages/Thanks/Thanks';

function App() {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={disclosure} />
        <Route exact path="/home" component={Frontend} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/forgotten" component={Forgotten} />
        <Route exact path="/reset/:id" component={ResetPassword} />
        <Route exact path="/klacht" component={Klacht} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/thanks" component={thanks} />
        <ProtectedRoute path="/admin" component={Admin} />
        <Route path="*" component={() => "404 NOT FOUND"} />
      </Switch>
    </div>
  );
}

const disclosure = () => {
  return (
    <>
      <h1>DIT IS EEN SCHOOLPROJECT</h1>
      <div style={{width: '100%', textAlign: 'center'}}>
        <Link to="/home">Naar de website</Link>
      </div>
    </>
  )
}

export default App;
