import React from 'react'

import Layout from '../../hoc/Layout'
import Button from '../../Components/UI/Button/Button'
import Input from '../../Components/UI/Input/Input'
import Spinner from '../../Components/UI/Spinner/Spinner'
import axios from 'axios'

const formValid = ({ formErrors, ...rest }) => {
    let valid = true;

    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
        val.length > 0 && (valid = false);
    });

    // validate the form was filled out
    Object.values(rest).forEach(val => {
        val === "" && (valid = false);
    });

    return valid;
};

class ResetPassword extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            email: "",
            password: "",
            confirmPassword: "",
            token: "",
            tokenValid: false,
            loading: true,
            succes: false,
            formErrors: {
                password: "",
                confirm: ""
            }
        }
    }

    componentDidMount() {
        axios.get('https://api.nsklacht.nl/api/password/find/' + this.props.match.params.id)
            .then((response) => {
                this.setState({ email: response.data.email, token: response.data.token, tokenValid: true, loading: false })
            }).catch((error) => {
                console.log(error)
                this.setState({ loading: false })
            })
    }

    inputChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let formErrors = { ...this.state.formErrors };

        this.setState({ [name]: value })

        switch (name) {
            case "password":
                formErrors.password =
                    value.length < 6 ? "Wachtwoord te kort (min. 6 tekens)" : "";

                formErrors.confirm =
                    this.state.confirmPassword === event.target.value ? "" : "Wachtwoorden komen niet overeen";

                break;
            case "confirmPassword":
                formErrors.password =
                    this.state.password.length < 6 ? "Wachtwoord te kort (min. 6 tekens)" : "";

                formErrors.confirm =
                    this.state.password === event.target.value ? "" : "Wachtwoorden komen niet overeen";
                break;
            default:
                break;
        }

        this.setState({ formErrors });
    }

    onSubmit = (event) => {
        event.preventDefault();

        if (formValid(this.state)) {

            this.setState({ loading: true })
            axios.post('https://api.nsklacht.nl/api/password/reset', {
                email: this.state.email,
                password: this.state.password,
                password_confirmation: this.state.confirmPassword,
                token: this.state.token
            })
                .then(response => {
                    console.log(response)
                    this.setState({ succes: true })
                })
                .catch(error => {
                    console.log(error);
                }).finally(() => {
                    this.setState({ loading: false })
                })
        }
    }

    render() {
        let tokenValid = this.state.tokenValid
        let email = this.state.email
        const { formErrors } = this.state;

        return (
            <Layout>
                {
                    this.state.loading ? <Spinner />
                        : <>
                            {
                                this.state.succes ? <h1>Wachtwoord is gewijzigd</h1>
                                    : <>
                                        <h1 className="title">Wachtwoord resetten</h1>
                                        {
                                            tokenValid ?
                                                <>
                                                    <h2>Voor: {email}</h2>
                                                    <form className="frontend-form" onSubmit={this.onSubmit}>
                                                        <Input 
                                                            name="password"
                                                            type="password"
                                                            change={this.inputChange}
                                                            logo="lock"
                                                            placeholder="nieuw wachtwoord"
                                                        />
                                                        <span className="errorMessage">{formErrors.password}</span>
                                                        <Input 
                                                            name="confirmPassword"
                                                            type="password"
                                                            change={this.inputChange}
                                                            logo="lock"
                                                            placeholder="wachtwoord bevestigen"
                                                        />
                                                        <span className="errorMessage">{formErrors.confirm}</span>

                                                        <div className="form-group buttons">
                                                            <Button variant="blue" onClick={this.onSubmit} type="submit">Wachtwoord wijzigen</Button>
                                                        </div>

                                                    </form>
                                                </>
                                                : <h2>Deze link is al gebruikt of is ongeldig</h2>
                                        }
                                    </>
                            }
                        </>
                }
            </Layout>
        )
    }
}

export default ResetPassword
