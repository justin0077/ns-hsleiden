import React, { useEffect } from 'react'
import { Switch, Route } from 'react-router-dom'
import axios from '../../axios-secure'

import AdminNav from '../../Components/Navigation/AdminNav/AdminNav'
import Dashboard from './Dashboard/Dashboard'
import Trein from './Trein/Trein'
import Auth from '../../hoc/Auth'

const Admin = (props) => {
    useEffect(() => {
        axios.get('/user')
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            });
    });

    const logout = () => {
        Auth.logout(() => {
            props.history.push("/login");
        });
    }

    const toTrain = (train) => {
        props.history.push('/admin/' + train);
    }

    return (
        <div>
            <AdminNav logout={logout} toTrain={toTrain}/>
            <div className="container">
            <Switch>
                <Route exact path="/admin" component={Dashboard} />
                <Route exact path="/admin/:trein" component={Trein} />
            </Switch>
            </div>
        </div>
    )
}

export default Admin
