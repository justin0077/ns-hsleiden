import React from 'react';

const klachtCard  = (props) => {
    return(
<div className="card">
    <div className="card-header">
        Treinstel nummer: {props.coupe}   
    </div>
    <div className="card-body">
        <div className="row dashboardSummery">
            <div className="col-3 mt-0"><span className="float-left"><b>Klacht:</b></span></div>
            <div className="col-8"><span className="float-right">{props.type}</span></div>
        </div>
        <div className="row dashboardSummery">
            <div className="col-3 mt-0"><span className="float-left"><b>Vertrek:</b></span></div>
            <div className="col-8"><span className="float-right">{props.van}</span></div>
            <div className="col-3 mt-0"><span className="float-left"><b>Aankomst:</b></span></div>
            <div className="col-8"><span className="float-right">{props.naar}</span></div>
        </div>
        <div className="collapse" id={props.klacht}>
            <div className="row dashboardSummery">
                <div className="col-3 mt-0"><span className="float-left"><b>Vertrektijd:</b></span></div>
                <div className="col-8"><span className="float-right">{props.tijdVan}</span></div>
                <div className="col-3 mt-0"><span className="float-left"><b>Aankomsttijd:</b></span></div>
                <div className="col-8"><span className="float-right">{props.tijdNaar}</span></div>
                <div className="col-3 mt-0"><span className="float-left"><b>Treinstel:</b></span></div>
                <div className="col-8"><span className="float-right">{props.coupe}</span></div>
                <div className="col-3 mt-0"><span className="float-left"><b>Verdieping:</b></span></div>
                <div className="col-8"><span className="float-right">{props.vedieping}</span></div>
            </div>
            <div className="row dashboardSummery">
                <div className="col-4"><span className="float-left"><b>Omschrijving:</b></span></div>
                <div className="col-8"><span className="float-right">{props.omschrijving}</span></div>
            </div>
        </div>
        <button className="btn btn-primary" type="button" data-toggle="collapse" data-target={"#" + props.klacht} aria-expanded="false" aria-controls={props.klacht}>
           Meer informatie
        </button>
    </div>
</div>
    )
}
export default klachtCard;