import React, {  useState, useEffect } from 'react'
import axios from '../../../axios-secure'
import Klachtcard from './Klachtcard.js';

const Dashboard = (props) => {
    const [klachten, setKlachten] = useState([]);

    useEffect(() => {
        axios.get('/klacht')
            .then(response => {
                console.log(response);
                setKlachten(response.data.klachten);            
            })
            .catch(error => {
                console.log(error.response);
            });
    }, [])

    return (
        <>
            <h1 className="title">Dashboard</h1>

            {/* <Klachtcard klacht="n1" />
            <Klachtcard klacht="n2" />
            <Klachtcard klacht="n3" /> */}

            {
                   klachten.map((klacht) =>{
                        return <Klachtcard key={klacht.id} 
                        klacht={"klacht" + klacht.id} 
                        id={klacht.id}
                        type={klacht.typeKlacht}
                        van={klacht.vertrekHalte}
                        naar={klacht.aankomstHalte}
                        tijdVan={klacht.vertrekTijd}
                        tijdNaar={klacht.aankomstTijd}
                        coupe={klacht.coupe}
                        verdieping={klacht.verdiepingCoupe}
                        omschrijving={klacht.klachtOmschrijving}
                        />
                    })   
            }

            </>
            )
    }

export default Dashboard;