import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

import './Login.css';
import Layout from '../../hoc/Layout'
import Logo from '../../Components/Logo'
import Button from '../../Components/UI/Button/Button'
import Spinner from '../../Components/UI/Spinner/Spinner';
import Input from '../../Components/UI/Input/Input'
import Auth from '../../hoc/Auth'


class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            username: "",
            password: "",
            remember: false,
            error: false,
            loading: false
        }

        if (localStorage.getItem('token') != null) {
            this.setState({loading: true})

            const token = localStorage.getItem('token')

            axios.get('https://api.nsklacht.nl/api/user', { headers: { 'Authorization': `Bearer ${token}` } })
                .then(response => {
                    Auth.login(token, () => {
                        this.props.history.push("/admin")
                    })
                })
                .catch(error => {
                    console.log(error);
                    localStorage.removeItem('token')
                }).finally(() => {
                    this.setState({loading: false})
                });
        }

        if (Auth.isAuthenticated()) {
            this.props.history.push("/admin");
        }
    }

    inputChange = (event) => {
        const name = event.target.name
        let value = ""

        if (name === "remember") {
            value = event.target.checked
        } else {
            value = event.target.value
        }


        this.setState({ [name]: value })
    }

    onSubmit = (event) => {
        event.preventDefault();
        this.setState({loading: true})
        const email = this.state.username
        const password = this.state.password
        const remember = this.state.remember


        axios.post('https://api.nsklacht.nl/api/login', {
            email: email,
            password: password
        })
            .then(response => {
                Auth.login(response.data.token, () => {
                    this.setState({ error: false })

                    if (remember) {
                        localStorage.setItem('token', response.data.token);
                    }

                    this.props.history.push("/admin")
                });
            })
            .catch(error => {
                console.log(error);
                this.setState({ error: true })
                this.setState({loading: false})
            });
    }

    render() {
        let error;

        if (this.state.error === true) {
            error = <div>Onjuiste gebruikernsaam of Wachtwoord</div>
        }

        return (
            <Layout>
                <Logo />
                <h1>Login Medewerker</h1>
                {
                    this.state.loading ? <Spinner />
                        : <form className="login-form" onSubmit={this.onSubmit}>

                            <Input 
                                name="username"
                                logo="account_circle"
                                type="text"
                                placeholder="Gebruikersnaam"
                                change={this.inputChange}
                            />

                            <Input 
                                name="password"
                                logo="lock"
                                type="password"
                                placeholder="Wachtwoord"
                                change={this.inputChange}
                            />

                            <div className="form-group remember">
                                <input className="remember" onChange={this.inputChange} name="remember" type="checkbox" />
                                <label className="logged-in">Ingelogd blijven</label>
                            </div>

                            <div className="form-group buttons">
                                <Button variant="blue" custom={{marginRight: '16px'}} clicked={this.onSubmit} type="submit">Login</Button>
                                <Link to="/register"><Button variant="white">Sign up</Button></Link>
                                {error}
                                <Link to="/forgotten" className="password-forget">Wachtwoord vergeten</Link>
                            </div>

                        </form>
                }
            </Layout>
        )
    }
}

export default Login