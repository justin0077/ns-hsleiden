import React from 'react'
import { Link } from 'react-router-dom'

import axios from 'axios'
import Layout from '../../hoc/Layout'
import Spinner from '../../Components/UI/Spinner/Spinner'
import Button from '../../Components/UI/Button/Button'
import Input from '../../Components/UI/Input/Input'

class Forgotten extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            email: "",
            loading: false,
            succes: false,
            error: ""
        }
    }

    inputChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;

        this.setState({ [name]: value })
    }

    onSubmit = (event) => {
        event.preventDefault();

        this.setState({ loading: true })
        axios.post('https://api.nsklacht.nl/api/password/create', {
            email: this.state.email,
        })
            .then(response => {
                console.log(response)
                this.setState({ succes: true })
            })
            .catch(error => {
                this.setState({ error: error.response.data.message })
            }).finally(() => {
                this.setState({ loading: false })
            })
    }

    render() {
        return (
            <Layout>
                {
                    this.state.loading ? <Spinner />
                        : <>
                            {
                                this.state.succes ? <h1>Email is verstuurd</h1>
                                    : <>
                                        <h1 className="title">Wachtwoord resetten</h1>
                                        <form className="frontend-form" onSubmit={this.onSubmit}>
                                            <Input 
                                                name="email"
                                                logo="email"
                                                type="email"
                                                change={this.inputChange}
                                                placeholder="Emailadres"
                                                autoComplete="email"
                                            />
                                            <div className="form-group buttons">
                                                <Button variant="blue" clicked={this.onSubmit} type="submit">Email versturen</Button>
                                            </div>
                                            <p>{this.state.error}</p>
                                            <Link to="/login" className="password-forget">Terug naar inloggen</Link>
                                        </form>
                                    </>
                            }
                        </>
                }
            </Layout>
        )
    }
}

export default Forgotten