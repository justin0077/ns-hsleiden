import React from "react";
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import Button from '../Components/UI/Button/Button';
import axios from 'axios'

function mapStateToProps(state){
  
  return{
      vertrekStation:state.vertrekStation,
      aankomstStation:state.aankomstStation,
      vertrekTijd:state.vertrekTijd,
      aankomstTijd:state.aankomstTijd,
      coupeNummer:state.coupeNummer,
      klachtKeuze:state.klachtKeuze,
      klachtBeschrijving:state.klachtBeschrijving,
      verdieping:state.verdieping,
  }
}



function Summary(props){
    const makeCall = (event) => {
      event.preventDefault();
      axios.post("https:/api.nsklacht.nl/api/klacht", {
        vertrekHalte: props.vertrekStation,
        aankomstHalte: props.aankomstStation,
        vertrekTijd: props.vertrekTijd,
        aankomstTijd: props.aankomstTijd,
        typeKlacht: props.klachtKeuze,
        coupe: props.coupeNummer,
        verdiepingCoupe: props.verdieping,
        klachtOmschrijving: props.klachtBeschrijving
      })
      .then(res => {
        console.log(res);
      })
      .catch(e => console.log(e.response))
    }

    return (
      <>
        <div className="form-group">
        <label htmlFor="username">Samenvatting</label>
        <div className="row fontVertrek">
          <div className="col-3 mt-0"><span className="float-left">Vertrek:</span></div>
          <div className="col-8"><span className="float-right">{props.vertrekStation}</span></div>
        </div>
        <div className="row fontVertrek">
          <div className="col-3 mt-0"><span className="float-left">Aankomst:</span></div>
          <div className="col-8"><span className="float-right">{props.aankomstStation}</span></div>
        </div>
        <div className="row fontVertrek">
          <div className="col-3 mt-0"><span className="float-left">Vertrektijd:</span></div>
          <div className="col-8"><span className="float-right">{props.vertrekTijd}</span></div>
        </div>
        <div className="row fontVertrek">
          <div className="col-3 mt-0"><span className="float-left">Aankomsttijd:</span></div>
          <div className="col-8"><span className="float-right">{props.aankomstTijd}</span></div>
        </div>
        <div className="row fontVertrek">
          <div className="col-3 mt-0"><span className="float-left">Treinstel:</span></div>
          <div className="col-8"><span className="float-right">{props.coupeNummer}</span></div>
        </div>
        <div className="row fontVertrek">
          <div className="col-3 mt-0"><span className="float-left">Klacht:</span></div>
          <div className="col-8"><span className="float-right">{props.klachtKeuze}</span></div>
        </div>
        <div className="row fontVertrek">
          <div className="col-3 mt-0"><span className="float-left">Verdieping:</span></div>
          <div className="col-8"><span className="float-right">{props.verdieping}</span></div>
        </div>
      </div>
      <Button variant="confirm" clicked={makeCall} type="button"><Link to="/thanks">Verstuur</Link></Button>
      </>
    );
}

export default connect(mapStateToProps)(Summary);
