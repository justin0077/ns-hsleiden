import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import './Register.css';
import Layout from '../../hoc/Layout'
import Logo from '../../Components/Logo';
import Spinner from '../../Components/UI/Spinner/Spinner'
import Button from '../../Components/UI/Button/Button'
import Input from '../../Components/UI/Input/Input'
import Auth from '../../hoc/Auth'
import axios from 'axios'

const emailRegex = RegExp(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
);

const formValid = ({ formErrors, ...rest }) => {  
    let valid = true;

    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
        val.length > 0 && (valid = false);
    });

    // validate the form was filled out
    Object.values(rest).forEach(val => {
        val === "" && (valid = false);
    });

    return valid;
};

class Register extends Component {

    constructor(props) {
        super(props)
        this.state = {
            username: "",
            email: "",
            password: "",
            confirmPassword: "",
            formErrors: {
                username: "",
                email: "",
                password: "",
                confirm: ""
            },
            loading: false
        }
        if (Auth.isAuthenticated()) {
            this.props.history.push("/protected");
        }
    }

    inputChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let formErrors = { ...this.state.formErrors };

        this.setState({ [name]: value })

        switch (name) {
            case "username":
                formErrors.username =
                    value.length < 4 ? "gebruikersnaam te kort (min. 4 tekens)" : "";
                break;
            case "email":
                formErrors.email = emailRegex.test(value)
                    ? ""
                    : "Ongeldig email adres";
                break;
            case "password":
                formErrors.password =
                    value.length < 6 ? "Wachtwoord te kort (min. 6 tekens)" : "";

                formErrors.confirm =
                    this.state.confirmPassword === event.target.value ? "" : "Wachtwoorden komen niet overeen";
                break;
            case "confirmPassword":
                formErrors.password =
                    this.state.password.length < 6 ? "Wachtwoord te kort (min. 6 tekens)" : "";

                formErrors.confirm =
                    this.state.password === event.target.value ? "" : "Wachtwoorden komen niet overeen";
                break;
            default:
                break;
        }

        this.setState({ formErrors });
    }

    onSubmit = (event) => {
        event.preventDefault();

        if (formValid(this.state)) {
            this.setState({ loading: true })
            axios.post('https://api.nsklacht.nl/api/register', {
                name: this.state.username,
                email: this.state.email,
                password: this.state.password,
            })
                .then(response => {
                    console.log(response);
                    this.props.history.push("/login")
                })
                .catch(error => {
                    console.log(error);
                    this.setState({ loading: false })
                });
        } else {
            console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
        }
    }

    render() {
        const { formErrors } = this.state;

        return (
            <Layout>
                <Logo />
                <h1>Account aanmaken</h1>
                {
                    this.state.loading ? <Spinner />
                    : <form onSubmit={this.onSubmit}>

                        <Input 
                            name="username"
                            logo="account_circle"
                            type="text"
                            placeholder="Gebruikersnaam"
                            change={this.inputChange}
                            autocomplete="username"/>
                        <span className="errorMessage">{formErrors.username}</span>
                        
                        <Input 
                            name="email"
                            logo="email"
                            type="email"
                            placeholder="Emailadres"
                            change={this.inputChange}
                            autocomplete="username"/>
                        <span className="errorMessage">{formErrors.email}</span>

                        <Input 
                            name="password"
                            logo="lock"
                            type="password"
                            placeholder="Wachtwoord"
                            change={this.inputChange}
                            autocomplete="new-password"/>
                        <span className="errorMessage">{formErrors.password}</span>

                        
                        <Input 
                            name="confirmPassword"
                            logo="lock"
                            type="password"
                            placeholder="Wachtwoord bevestigen"
                            change={this.inputChange}
                            autocomplete="new-password"/>
                        <span className="errorMessage">{formErrors.confirm}</span>

                    <div className="form-group buttons">
                        <Button variant="blue" onClick={this.onSubmit} type="submit">Sign up</Button>
                        <Link to="/login" className="password-forget">Heeft u al een account? login</Link>
                    </div>
                </form>
                }
        </Layout>
        )
    }
}

export default Register