import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import './Home.css';
import Layout from '../../hoc/Layout'
import Logo from '../../Components/Logo'
import Button from '../../Components/UI/Button/Button'
import Input from '../../Components/UI/Input/Input'

const emailRegex = RegExp(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
);

class Frontend extends Component {
    constructor(props) {
        super(props)
        this.state = {
          email: "Voorbeeld@gmail.com"
        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({email: event.target.value});
    }

    render() {
        const validEmail = emailRegex.test(this.state.email) ? true : false
        return (
            <Layout background>
                <Logo />
                <h1 className="title">Welkom bij de NS Overlast Melder</h1>
                <h2>Vul hier uw e-mail in en meldt de overlast</h2>
                <form className="frontend-form" onSubmit={this.onSubmit}>
                    <Input
                        name="e-mail"
                        logo="email"
                        type="text"
                        placeholder="Voorbeeld@gmail.com"
                        change={this.handleChange}
                    />
                    {!validEmail ? "E-mail is ongeldig" : <br />}

                    <div className="form-group">
                        <Link to="/klacht"><Button variant="blue" clicked={this.onSubmit} disabled={!validEmail || this.state.email === "Voorbeeld@gmail.com"} type="submit">Volgende</Button></Link>
                    </div>

                </form>
            </Layout>
        );
    }
}

export default Frontend
